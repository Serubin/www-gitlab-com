---
layout: job_family_page
title: "Senior Sales Development Manager, Acceleration"
---

As a Senior Manager of the Sales Development Acceleration team, you are a player-coach. As a manager your job is threefold: (1) lead from the front (the “tip of the spear”) and generate qualified opportunities for the sales team, (2) train other members of the Sales Development team, and (3) take on operational and  administrative tasks to help the Sales Development team perform and exceed expectations. You will be a source of knowledge and best practices amongst the outbound Sales Development Representatives (SDRs) and will help to train, onboard, and mentor new SDRs.
This team will be focused on the enterprise acceleration space. It will be responsible for developing plans in conjunction with Field and Digital Marketing to light up underserved territories and execute to pipeline deficiencies as needed. The successful candidate will have the ability to determine the go to market strategy as well as hire the initial global team. This is an opportunity to build a new functionality within the GitLab Revenue Marketing team.

## Responsibilities
- Build and train other members of the Sales Development team to identify, contact, and create qualified opportunities
- Determine a strategy for acquisition and create a process for execution
- Help to define performance metrics by which SDRs will be measured
- Establish a team of experienced hunters
- Create a process for continuous development and onboarding new hires
- Coach SDRs through shadowing, role plays, process improvement, and performance reporting in recurring 1:1’s
- Work closely with the Online Marketing Manager on targeted ad campaigns for the team’s account list
- Work closely with the Sales and Business Development Managers to improve opportunity management and qualification processes
- Work closely with the Sales and Business Development Managers as well as the Regional Sales Directors (RDs) to identify key company accounts to develop
- Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to your assigned accounts
- Collaborate with Marketing Operations to identify new tools and programs that could help improve output and revenue generation
- Assist with recruiting, hiring, and onboarding new Sales Development Representatives

## Requirements
- A prospecting mindset
- Proven success in acquisition and closing roles
- Basic understanding of marketing programs and field marketing concepts
- Coaching or management experience
- Experience with CRM software and prospecting tools like Outreach, Drift, DiscoverOrg etc.
- Experience in sales operations and/or with marketing automation software preferred
- Understanding of B2B software, Open Source, and the developer product space is preferred
- Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
- You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
- Passionate about technology and learning more about GitLab
- Be ready to learn how to use GitLab and Git
- You share our values, and work in accordance with those values.
- Leadership at GitLab
- Ability to use GitLab
