---
layout: handbook-page-toc
title: Acquisition and Conversion Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Acquisition and Conversion Groups are part of the [Growth section]. The Acquisition Group is responsible for promoting the value of GitLab and accelerating site visitors transition to happy valued users of our product. The Conversion Group is responsible for continually improving the user experience when interacting with locked features, limits and trials.

**I have a question. Who do I ask?**
Questions should start by @ mentioning the Product Manager for the [Acquisition group](/handbook/product/categories/#acquisition-group), the [Conversion group](/handbook/product/categories/#conversion-group), or by creating an issue in the [Growth issues board].

## How we work

* We're data savvy
* In accordance with our [GitLab values]
* Transparently: nearly everything is public
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

## Workflow

Our development process follows the [Engineering workflow](/handbook/engineering/workflow/). 

### Cadence

There are two cadences we operate on: a [Monthly Milestone Cadence](/handbook/engineering/development/growth/acquisition-conversion/index.html#monthly-milestone-cadence) for feature development and a [Weekly Experiment Cadence](/handbook/engineering/development/growth/acquisition-conversion/index.html#weekly-experiment-cadence) for high-velocity experimentation.

Each group will decide which cadence to follow depending on the work defined by the Product Manager:
* The Acquisition Group currently follows a monthly milestone cadence.
* The Conversion Group currently follows a weekly experiment cadence.

#### Monthly Milestone Cadence
In a monthly milestone cadence, we plan and deliver work on a monthly cycle using [milestones](https://docs.gitlab.com/ee/user/project/milestones/) and following the [product development timeline](/handbook/engineering/workflow/#product-development-timeline). 

To ensure we adhere to this monthly cycle, we pay special attention to these dates:
* `M-1, 13th`: Milestone commitment finalized
* `M-1, 18th`: Begin milestone
* `M, 17th`: End milestone
* `M, 19th`: Begin group retro
* `M, 22nd`: Release day
* `M, 26th`: End group retro

We plan and groom issues in advance of finalizing a milestone commitment on `M-1, 13th` and before beginning development on `M-1, 18th`. 

##### Milestone Commitment

A milestone commitment is a list of issues our team aims to complete in the milestone. 

Our milestone commitments are based off of our team's estimated capacity, which is the sum of issue weights completed in the last milestone. If there is a large variation in the estimated capacity of the last milestone and the one before it, we will use an average estimated capacity of the last few milestones.

Once issues are groomed, estimated, and prioritized, we begin loading issues into a milestone commitment until the sum of the loaded issue weights equals our team's estimated capacity. The loaded issues are our milestone commitment and are tagged with the correct milestone label.

#### Weekly Experiment Cadence
In a weekly experiment cadence, we plan and deliver work on a weekly cycle. 

We follow a four step process as outlined by [Andrew Chen's How to build a growth team.](https://andrewchen.co/how-to-build-a-growth-team/) 
1. **Form Hypotheses:** Define ideas our team wants to test.
2. **Prioritize Ideas:** Decide which ideas to test first.
3. **Implement Experiments:** Do the Product, Design, Engineering, Data, and Marketing work to execute the experiment. 
4. **Analyze Results:** Dive into the results data and prove or disprove our hypotheses. 

Each week, we provide progress updates and talk about our learnings in our [Growth Weekly meeting](/handbook/product/growth/#weekly-growth-meeting).

Our aim is to implement one new experiment per week. The duration of each experiment will vary depending on how long it takes for experiment results to reach statistical significance. Due to the varying duration, there will be some weeks where we have several experiments running concurrently in parallel.

##### Broader Team Integration

Some of our work will need to be integrated into the [monthly milestone cadence](/handbook/engineering/workflow/#product-development-timeline) which the broader engineering and product organizations.

Examples of this include: 
* Monthly releases for self-hosted instances
* [Monthly release posts](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-to-a-section)
* [Monthly team retrospectives](/handbook/engineering/management/team-retrospectives/)

To help with this integration, our experiments will still be tagged with the correct milestones. 

### How We Use Issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses issues.

#### Issue Boards

We use **group level issue boards** for visibility into all nested projects in a group. We use **workflow boards** to track issue progress throughout a milestone cycle. We use **milestone boards** for high level planning across several milestones.

##### gitlab.com/gitlab-org

The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [Gitlab](https://gitlab.com/gitlab-org/gitlab), [Customers](https://gitlab.com/gitlab-org/customers-gitlab-com), and [License](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- [Acquisition Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
- [Acquisition Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
- [Conversion Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
- [Conversion Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)

##### gitlab.com/gitlab-services

The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [Versions](https://gitlab.com/gitlab-services/version-gitlab-com) project.
- Acquisition Workflow Board
- Acquisition Milestone Board
- Conversion Workflow Board
- Conversion Milestone Board

#### Issue Creation

We aim to create issues in the same project as where the future merge request will live. For example, if an experiment is being run in the Customers application, both the issue and MR should be created in the Customers project. 

We emphasize creating the issue in the right project to avoid having to close and move issues later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

We aim to maintain a 1:1 ratio between issues and merge requests. For example, if one issue requires two merge requests, we will split the issue into two issues. We aim for a 1:1 ratio as we believe it emphasizes keeping MRs small, it makes estimation on issues straight forward, and it provides more visibility into an issue's progress.

#### Issue Hierarchy
There are two ways to organize issue hierarchy: Epics and Parent Issues.

Depending on their preference, each group's Product Manager will decide which issue hierarchy to use:
* The Acquisition Group currently uses Epics.
* The Conversion Group currently uses Parent Issues.

##### Epics
[Epics](https://docs.gitlab.com/ee/user/group/epics/) are a Gitlab feature that allows easy linking of higher-level themes across issues. Epics have a roadmap gantt chart which is useful for visualizing work.

##### Parent Issues
Parent issues are normal Gitlab issues that are used to group several child issues together. Since Parent issues are normal issues, they have the full functionality of issues including the ability to add labels, add milestones and assign team members.

#### Issue Labels
We follow the issue label definitions in the [issue workflow](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md) in addition to the ones mentioned below:

##### Workflow Labels
Our groups utilize a set of three newly created experiment workflow labels in conjunction with the existing workflow labels. 
- `open`
- `~"workflow::validation backlog"`
- `~"workflow::problem validation"`
- `~"workflow::experiment writeup ready"` (newly created)
- `~"workflow::design"`
- `~"workflow::experiment ready"` (newly created)
- `~"workflow::solution validation"`
- `~"workflow::ready for development"`
- `~"workflow::in dev"`
- `~"workflow::in review"`
- `~"workflow::verification"`
- `~"workflow::experiment active"` (newly created)
- `closed`

##### Experiment Results Labels
To help our group track the results of our experiments, we utilize the following labels:
- `~"experiment_result::validated"` (newly created)
- `~"experiment_result::invalidated"` (newly created)
- `~"experiment_result::inconclusive"` (newly created)
- `~"experiment_result::validated::clean_up_for_production"` (newly created)

##### Milestone Week Labels
To help our group prioritize experiments within a milestone, we utilize the following labels:
- `~"milestone::week_1"` (newly created)
- `~"milestone::week_2"` (newly created)
- `~"milestone::week_3"` (newly created)
- `~"milestone::week_4"` (newly created)

##### Release Scoping Labels
When following a monthly milestone cadence, the top 70% of a milestone's issues will be labelled as `Deliverable` while the remaining 30% of issues will be labelled as `Stretch`.

If a single deliverable is broken in to mutliple smaller issues, one of these issues should be labelled as the `Deliverable` while the other issues are dependencies and are linked to the `Deliverable` issue. Once all dependency issues have been delivered, the issue with the `Deliverable` label can be closed.

### Prioritization
Prioritization is a collaboration between Product, UX, Data, and Engineering. We use the
[ICE framework](/direction/growth/#growth-ideation-and-prioritization) for experiments. Each `Deliverable` issue will have an ICE score.

### Estimation
We use a light-weight estimation process using a fibonacci scale with a maximum weight of 5. Anything over a 5 (large) indicates the work should be broken down into smaller, clearly defined issues. 

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | Trivial: The simplest possible change. We are confident there will be no side effects. |
| 2 | Small: A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | Medium: A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | Large: A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |

In planning and estimation, we value [velocity over predictability](https://about.gitlab.com/handbook/engineering/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (MR throughput) enables our Growth teams to achieve a [weekly experimentation cadence](https://about.gitlab.com/handbook/product/growth/#weekly-growth-meeting). 

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

### Daily Standups (Async)

We have daily asynchronous standups using [status hero's](https://statushero.com/integrations/gitlab) Slack integration. The purpose of these standups are to allow team members to have visibility into what everyone else is doing, allow a platform for asking for and offering help, and provide a starting point for some social conversations.

Three questions are asked at each standup:
* How do you feel today?
* What are you working on today?
  * Our status updates consist of the various issues and merge requests that are currently being worked on. If the work is in progress, we provide the estimated percentage complete.
  > `!12345 - 50% complete` - Working on tests and refactoring
* Any blockers?
  * We raise any blockers early and ask for help.

### Meetings (Sync)

Each group holds synchronus meetings twice a week to gain additional clarity and alignment on our async discussions.

* The Acquisition Group meets on Mondays 03:30pm UTC and Wednesdays 03:30pm UTC
* The Conversion Group meets on Tuesdays 02:00pm UTC and Wednesdays 04:00pm UTC

The agenda for each meeting follows this structure:
* In Development
  * Blockers
  * Existing Experiments
* Planning
  * Grooming
  * Design Review
  * Estimation
  * Milestone Commitments
* Retro 
  * What went well 
  * What didn’t go well
  * What can be improved

## Measuring Throughput

One of our main engineering metrics is [throughput](https://about.gitlab.com/handbook/engineering/management/throughput/) which is the total number of MRs that are completed and in production in a given period of time. We use throughput to encourage small MRs and to practice our values of [iteration](https://about.gitlab.com/handbook/values/#iteration). Read more about [why we adoped this model](https://about.gitlab.com/handbook/engineering/management/throughput/#why-we-adopted-this-model).

We aim for 10 MRs per engineer per month which is tracked using our [throughput metrics dashboard](https://app.periscopedata.com/app/gitlab/559676/Growth:Acquisition-and-Conversion-Development-Metrics).

## Team Members

The following people are permanent members of the Acquisition and Conversion Group:

<%= direct_team(manager_role: 'Engineering Manager, Growth:Acquisition and Conversion') %>

## Common Links

* [Growth section]
* [Growth issues board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth)
* [Growth Performance Indicators]
* [Fulfillment issues board]
* `#g_fulfillment` in [Slack](https://gitlab.slack.com/archives/g_fulfillment)
* [Telemetry issues board]
* `#g_telemetry` in [Slack](https://gitlab.slack.com/archives/g_telemetry)
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[GitLab values]: /handbook/values/
[Growth section]: /handbook/engineering/development/growth/
[Growth issues board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product
[Growth meetings and agendas]: https://docs.google.com/document/d/1QB9uVQQFuKhqhPkPwvi48GaibKDwGAfKKqcF-s3Y6og

[Growth Performance Indicators]: /handbook/engineering/development/growth/performance-indicators/

[Fulfillment issues board]: https://gitlab.com/gitlab-org/fulfillment/issues
[Telemetry issues board]: https://gitlab.com/gitlab-org/telemetry/issues
