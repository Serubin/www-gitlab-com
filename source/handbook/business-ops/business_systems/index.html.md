---
layout: handbook-page-toc
title: "Business System Analysts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Business Systems Analyst

### Links
*  [Job Description](/job-families/finance/business-system-analyst/)
*  [Epic Board: Business Operations](https://gitlab.com/groups/gitlab-com/business-ops/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=BSA)

### General Ongoing
##### Documentation
For reference, "[What Nobody Tells you about Documentation](https://www.divio.com/blog/documentation/)"
* Tutorials
* How-To Guides
* Explanation
* **Reference** (most of the type we create)

##### IT Capability Matrix
As IT as a department scales and matures, [we must identify how we grow and operate](https://docs.google.com/spreadsheets/d/1AEQBweIZaoI-Z43MkzmL30WLhnmzw3UR3VNtgoDzSPM/edit#gid=0) inline with company values and business requirements.

##### Retrospectives
We can host your project retrospective.

##### Tool Evaluation
We can provide [templates](https://drive.google.com/open?id=18b0MYkPYFAzBn3tXB2DbMOrBY3zPRLQa) for vendor "Request for Proposals" and user stories; we can help review your user stories.
We are also happy to be available for demos and would like to participate in tool evaluation that integrates with other tools or intersects with multiple departments.
   *  [Template: Request for Proposal](https://docs.google.com/document/d/1_Q2b5opYUQ9TlGmF2vOJ6anu0spVFMkNO6YCR4UjYXM/edit?usp=sharing)
   *  [Template: User Stories](https://docs.google.com/spreadsheets/d/1c1R0pqKr8YwXXATzFVEUaofF2luNrHbmcNkKAWisebs/edit?usp=sharing)

### Focused Attention

##### Business Analysis Support
Phil Encarnacion, Business Systems Specialist

##### Finance and People Operations
Barbara R., Business Systems Analyst
*  Scale onboarding

##### Portal integrations and operations
Jamie Carey, Senior Business Systems Analyst
*  Portal Analysis and Documentation
*  Go to market operations

##### Security and Compliance
Karlia Kue, Business Systems Analyst
*  Offboarding compliance
*  GPPR

##### IT Operations
Lis Vinueza, Business Systems Analyst
*  Embed Tech stack in Handbook
*  Automation of system provisioning
*  Tech Stack: Maintenance
*  Zendesk <> SFDC integration